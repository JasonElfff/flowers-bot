from os import getenv, path
from discord import app_commands, File
import logging
from re import compile

log = logging.getLogger('discord')
OUTDIR = getenv('OUT_DIR')
from utils.get_flowers import get_flowers
from utils.random_unicode_flower import get_random_unicode_flower

illegal_chars_re = compile(r".*[<$();:#&].*")

@app_commands.autocomplete(flower=get_flowers)
@app_commands.command(name = "get_flower", description = "Gimme the flower")
async def get_flower(interaction, flower: str):
    log.info(f"/flower-get has been called by { interaction.user.name} with arg {flower}.")
    if illegal_chars_re.search(flower):
        log.info(f"Illegal name : {flower}")
        await interaction.response.send_message(content=f"Le nom saisi contient des caractères non-autorisés : **{flower}**.")
        return
    if path.exists(f"{OUTDIR}/{interaction.user.name}/{flower}"):
        random_flower = get_random_unicode_flower()
        msg = f"{random_flower} Et voilà ta fleur ! {random_flower}"
        await interaction.response.send_message(content=msg, file=File(f"{OUTDIR}/{interaction.user.name}/{flower}"))
    else:
        await interaction.response.send_message(content=f"Désolé, aucune image avec le nom {flower} n'a été trouvé")
