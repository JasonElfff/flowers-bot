from os import getenv, path, rename
from discord import app_commands, Interaction, Attachment, File
from re import compile
from utils import get_random_unicode_flower, create_user_dir
import logging
from imghdr import what
from redis import Redis
from json import dumps, loads

illegal_chars_re = compile(r".*[<$();:#&].*")

OUTDIR = getenv('OUT_DIR')
log = logging.getLogger('discord')

@app_commands.command(name = "add_flower", description = "Add a new flower to your inventory")
async def add_flower(interaction: Interaction, file: Attachment, flower_name: str):
    log.info(f"/add_flower has been called by {interaction.user.name}.")

    # Detect illegal user input
    if illegal_chars_re.search(flower_name):
        log.info(f"Illegal name : {flower_name}")
        await interaction.response.send_message(content=f"Le nom choisi contient des caractères non-autorisés : **{flower_name}**.")
        return

    # Create user directory on disk if not exists
    if not path.exists(f"{OUTDIR}/{interaction.user.name}"):
        create_user_dir(interaction.user.name)

    # Determine image format and thus, filename
    filename = f"{OUTDIR}/{interaction.user.name}/{flower_name}"
    await file.save(fp=filename.format(file.filename))
    filetype = what(filename)
    rename(filename, f"{filename}.{filetype}")

    # Store reference to the image in db
    r = Redis('redis', 6379, charset="utf-8", decode_responses=True)
    if not r.get(f"{interaction.user.name}"):
        new_list = []
        r.set(f"{interaction.user.name}", dumps(new_list))
    user_image_list = loads(r.get(f"{interaction.user.name}"))
    user_image_list.append(f"{path.basename(filename)}.{filetype}")
    r.set(f"{interaction.user.name}", dumps(user_image_list))

    # Answer the user
    flower = get_random_unicode_flower()
    await interaction.response.send_message(file=File(f"{filename}.{filetype}"), content=f"**{flower}Une belle fleur !{flower}** Enregistrée dans ton inventaire sous le nom de {flower_name}.")

