from os import getenv, path, rename, listdir
from discord import app_commands, Interaction, Attachment, File
from utils import get_random_unicode_flower, create_user_dir
from random import choices, choice
from string import ascii_uppercase, digits
import logging
from imghdr import what
from PIL import Image

OUTDIR = getenv('OUT_DIR')
APPDIR = getenv('APP_DIR')
log = logging.getLogger('discord')

@app_commands.command(name = "flowerize", description = "Ajoute des fleurs à une image !")
async def flowerize(interaction: Interaction, file: Attachment):
    log.info(f"/flowerize has been called by {interaction.user.name}.")
    if not path.exists(f"{OUTDIR}/{interaction.user.name}"):
        create_user_dir(interaction.user.name)
    random_filename = ''.join(choices(ascii_uppercase + digits, k=50))
    filename = f"{OUTDIR}/{interaction.user.name}/{random_filename}"
    await file.save(fp=filename.format(file.filename))
    filetype = what(filename)
    rename(filename, f"{filename}.{filetype}")
    flower = get_random_unicode_flower()

    bg = Image.open(f"{filename}.{filetype}")
    random_banner =  choice(listdir(f"{APPDIR}/static/floral_banners"))
    fg = Image.open(f"{APPDIR}/static/floral_banners/{random_banner}")
    resized_fg = fg.resize(bg.size)
    bg.paste(resized_fg, (0,0), resized_fg)
    random_filename = ''.join(choices(ascii_uppercase + digits, k=50))
    bg.save(f"{OUTDIR}/{interaction.user.name}/{random_filename}.png")

    await interaction.response.send_message(file=File(f"{OUTDIR}/{interaction.user.name}/{random_filename}.png"), content=f"**{flower}Et voilà ta nouvelle image !{flower}**")

