from os import listdir, getenv
from random import choice, random
from discord import app_commands, File
from utils.random_unicode_flower import get_random_unicode_flower
import logging
log = logging.getLogger('discord')
APPDIR = getenv('APP_DIR')

@app_commands.command(name = "asian_flower", description = "Returns a random asian flower")
async def asian_flower(interaction):
    log.info(f"/asian_flower has been called by {interaction.user.name}")
    flower_path = choice(listdir(f"{APPDIR}/static/asian_flowers"))
    random_flower = get_random_unicode_flower()
    msg = f"{random_flower} Tiens, ta belle fleur ! {random_flower}"
    await interaction.response.send_message(content=msg, file=File(f"{APPDIR}/static/asian_flowers/{flower_path}"))
