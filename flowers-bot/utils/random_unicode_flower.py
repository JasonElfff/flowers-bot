from random import choice
def get_random_unicode_flower():
    flowers = [ '🌹', '🌷', '⚘', '🌺', '🌻', '🌼', '💐', '🪷', '🪻', '💮', '🥀' ]
    return choice(flowers) 
