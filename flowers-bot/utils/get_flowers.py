from discord import app_commands, Interaction
from typing import List
from os import getenv, listdir, path

async def get_flowers(
    interaction: Interaction,
    searched: str,
) -> List[app_commands.Choice[str]]:
    
    OUTDIR = getenv('OUT_DIR')
    user_dir = f"{OUTDIR}/{interaction.user.name}"
    if not path.exists(user_dir):
        pass
    flowers = listdir(user_dir) 
    
    matching_flowers = []
    for flower in flowers:
        next_flower = False
        for term in searched.lower().split(' '):
            if not term in flower.lower():
                next_flower = True
                continue
        if next_flower:
            continue
        matching_flowers.append(flower)

    return [
        app_commands.Choice(name=flower, value=flower)
        for flower in matching_flowers
        ][:20]

