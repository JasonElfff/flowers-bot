import logging
from sys import stdout

def logger():
    stdout_handler = logging.StreamHandler(stream=stdout)
    handlers = [stdout_handler]

    logging.basicConfig(
        level=logging.INFO, 
        format='[%(asctime)s] {{%(filename)s:%(lineno)d} %(levelname)s - %(message)s',
        handlers=handlers
    )
