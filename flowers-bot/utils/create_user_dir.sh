#!/bin/bash
# Create userdir if it does not exist

## VARS
### Formatting
declare -r BOLD='\e[1m'
declare -r RED='\e[31m'
declare -r GREEN='\e[32m'
declare -r YELLOW='\e[33m'
declare -r BLUE='\e[34m'
declare -r NC='\e[0m'
### Internal structures
declare -r output_dir='/app/output'
declare -r logfile='/app/output/create_user_dir.log'
### argv
declare -r given_user="${1}"

## FUNCS
log() {
  log_level=$1
  if [[ "${log_level}" == 'ERROR' ]] ; then
    COLOR="${RED}" 
  elif [[ "${log_level}" == 'WARNING' ]] ; then
    COLOR="${YELLOW}" 
  elif [[ "${log_level}" == 'NOTICE' ]] ; then
    COLOR="${BLUE}" 
  elif [[ "${log_level}" == 'SUCCESS' ]] ; then
    COLOR="${GREEN}"
  elif [[ "${log_level}" == 'INFO' ]] ; then
    COLOR="${WHITE}"
  else 
    echo "Don't know the following log-level : ${log_level}."
  fi

  timestamp=$(date +%Y%m%d-%H%M%S)
  log_msg="${2}"
  echo -e "[${timestamp}] ${BOLD}${COLOR}[${log_level}]${NC} ${log_msg}" # | tee -a "${logfile}" # do not log in text file at the moment
}

path_sanitize() {
  path="${1}"
  if [[ "${path}" == */* ]]; then
    log ERROR "Input must not contain any '/', exiting."
    exit 1
  fi
}

preflight_checks() {
  [[ ! -d "${output_dir}" ]] && log ERROR "Output dir ${output_dir} does not exist, exiting." && exit 1
  [[ ! -w "${output_dir}" ]] && log ERROR "Output dir ${output_dir} is not writable by user ${USER} with UID ${UID}." && exit 1
  [[ ! -f "${logfile}" ]] && log ERROR "Logfile ${logfile} does not exist, exiting." && exit 1
  [[ ! -w "${logfile}" ]] && log ERROR "Logfile ${logfile} is not writable by user ${USER} with UID ${UID}." && exit 1
}

## CODE
log INFO "$0 has been called with ${given_user}"
preflight_checks
if [[ -d "${output_dir}/${given_user}" ]] ; then
  log INFO "Directory for user ${given_user} already exists."
else
  log INFO "Directory for user ${given_user} does not exist. Creating..."
  path_sanitize "${given_user}"
  if sh -c "mkdir ${output_dir}/${given_user}"; then
    log SUCCESS "Directory for user ${given_user} has been successfully created."
  else
    log ERROR "Creation of directory ${output_dir}/${given_user} failed. Exiting."
    exit 1
  fi
fi
