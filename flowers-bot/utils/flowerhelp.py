from os import getenv
import logging
from discord import app_commands
APPDIR = getenv('APP_DIR')
log = logging.getLogger('discord')

from utils.random_unicode_flower import get_random_unicode_flower

@app_commands.command(name = "flowers_help", description = "Affiche l'aide du bot floral !")
async def flowers_help(interaction):
    flowers_message = open(f"{APPDIR}/static/txt/flowers_help","r").read()
    log.info(f"/flowers-help has been called by { interaction.user.name}.")
    random_flower = get_random_unicode_flower()
    help_message = f"{random_flower} Hellomeo copain {interaction.user.mention}{flowers_message}"
    await interaction.response.send_message(content=help_message)

