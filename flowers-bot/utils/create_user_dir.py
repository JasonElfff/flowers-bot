from os import getenv, system
import logging
log = logging.getLogger('discord')

def create_user_dir(username):
  log.info(f"Calling user creation shellscript.")
  system(f"/app/flowers-bot/utils/create_user_dir.sh {username}")
