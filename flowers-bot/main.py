# client.py

from os import getenv, mkdir, path
from dotenv import load_dotenv
from discord import Intents, app_commands, Client
import logging
logger = logging.getLogger('discord')

load_dotenv()
TOKEN  = getenv('DISCORD_TOKEN')
GUILD  = getenv('DISCORD_GUILD')
OUTDIR = getenv('OUT_DIR')
if not path.exists(OUTDIR):
  mkdir(OUTDIR)

intents = Intents.all()
client = Client(intents=intents)
tree = app_commands.CommandTree(client)

from inventory import add_flower, get_flower
from flower_library import beautiful_flower, asian_flower
from utils import flowers_help
from flowerize import flowerize

@client.event
async def on_ready():

    commands = [add_flower, get_flower, beautiful_flower, asian_flower, flowers_help, flowerize]
    for command in commands:
        tree.add_command(command)

    await tree.sync()

    for guild in client.guilds:
        print(guild.name)
        logger.info(f'{client.user} is connected to the following guild:\n'
            f'{guild.name}(id: {guild.id}')
        await tree.sync(guild=guild)
        break
        
client.run(TOKEN)
