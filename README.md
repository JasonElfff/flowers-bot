# flowers-bot

🎉🎉🎉 NEWS 🎉🎉🎉

**We're happy to announce the release of a new version of our bot today as of 13/02/2024 !**
And we have also opened another instance of the bot, because of very high usage (thankyou guys !) hosted under **our famous server `jasonelf.tech`** !

We love flowers too !

🎉🎉🎉🎉🎉🎉
      
## Run the bot

To use run the bot :

```bash
# Clone the repo
$ git clone <this repo>

# Create the output dir and set permissions
# This is used for persistent data
$ cd <this_repo>
$ mkdir output
$ chown -R 2789:2789 output

# Set the discord token
$ vim flowers_bot/.env

# Run the bot (for development purposes)
$ docker compose -f docker-compose-dev.yml up --build 
```
