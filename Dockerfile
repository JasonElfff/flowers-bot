FROM python:3.10.10-bullseye

RUN groupadd -g 2789 flowers-bot \
    && useradd -m -u 2789 -g 2789 -s /bin/bash flowers-bot

ARG flag

COPY . /app

RUN echo $flag > /app/flag \
    && chown root:root /app/flag \
    && chmod 444 /app/flag

WORKDIR /app/flowers-bot

USER flowers-bot

RUN pip install -r requirements.txt

CMD [ "python", "main.py" ]
